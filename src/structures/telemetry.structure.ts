export interface Telemetry{
    messageId : String,
    humidity : String,
    temperature : String
}