import { Injectable, isDevMode, OnInit, Component, ChangeDetectionStrategy } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http'
import {Subject, Observable, interval} from 'rxjs'
import {environment} from '../../environments/environment'
import { Coords } from 'src/structures/coords.structure';
import { Telemetry } from 'src/structures/telemetry.structure';
import {map,zip, delay, debounceTime} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CurrentWeatherService implements OnInit{
  public weatherSubject : Subject<any> = new Subject<any>();
  public weather$ : Observable<any>;

  private httpHeaders = new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  });

  endpoint:string = 'http://localhost:8080/telemetry/all';
  constructor(private http : HttpClient) { 
    this.weather$ = this.weatherSubject.asObservable().pipe(
      map((data : any)=>{
        console.log(data.length);
        let telemetry : Telemetry={
          ...data[data.length-1]
        }
        return telemetry
      })
    );
this.get1();  
}
  

get1(){     
    let url = this.endpoint
    if (false){
      url = 'assets/telemetry.json'
    
    }
    this.http.get(url).subscribe(this.weatherSubject)
}

ngOnInit(){

}

}
  


  
  

