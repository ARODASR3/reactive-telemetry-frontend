import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { CurrentWeatherService } from '../services/current-weather.service';
import { observable, Observable, Subscription, interval } from 'rxjs';
import { Telemetry } from 'src/structures/telemetry.structure';
import { timer } from 'rxjs';

@Component({
  selector: 'app-current-telemetry',
  templateUrl: './current-telemetry.component.html',
  //template: '<div class="card" *ngIf = "weatherService.weather$ | async as weather"> <div>{{weather.messageId}}</div>  <div>{{mid}}</div> <button (click)="click()">Click me</button> <div>',
  styleUrls: ['./current-telemetry.component.css'],
  changeDetection:ChangeDetectionStrategy.Default
})

export class CurrentTelemetryComponent implements OnInit {

  mid:string;
 
  constructor(public weatherService : CurrentWeatherService) { 
  }
  
  public observable :Observable<any>;
  private updateSubscription: Subscription;
  
  
  ngOnInit() {
  this.weatherService.weather$.subscribe(console.log);

  this.weatherService.weather$.subscribe(
    telemetry => this.mid = telemetry.messageId
  );
  } 
 

}
