import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentTelemetryComponent } from './current-telemetry.component';

describe('CurrentTelemetryComponent', () => {
  let component: CurrentTelemetryComponent;
  let fixture: ComponentFixture<CurrentTelemetryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentTelemetryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentTelemetryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
